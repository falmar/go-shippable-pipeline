#!/bin/bash -e

HUB_USR=$HUB_INTEGRATION_USERNAME
HUB_PWD=$HUB_INTEGRATION_PASSWORD
IMAGE=$1
SOURCE=$2
BRANCH=$3
COMMIT=$4

login() {
  docker login -u $HUB_USR -p $HUB_PWD
}

build_image() {
  docker build -t $SOURCE:$COMMIT $REPO_SRC
  docker push $SOURCE:$COMMIT
}

createOutState() {
  echo versionName=$COMMIT > $JOB_STATE/$IMAGE"_"$BRANCH.env
  cat /build/state/$IMAGE"_"$BRANCH.env
}


login
build_image
createOutState
